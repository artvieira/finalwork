create or replace trigger trg_data_pedido
before insert on pedido
referencing new as new old as old
for each row
declare
begin
    :new.data_criacao := sysdate;
    :new.usuario_criador := sys_context('USERENV', 'SESSION_USER');
end;

create view estoque_produto AS 
select e.id as id_estoque, 
    e.gondola, 
    p.id as id_produto 
from produto p
inner join estoque e
    on p.id_estoque = e.id;
    
select * from estoque_produto;
