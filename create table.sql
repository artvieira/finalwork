drop table item_pedido;
drop table produto;
drop table estoque;
drop table pedido;

create table pedido (
	id NUMBER GENERATED ALWAYS AS IDENTITY,
	data_criacao timestamp,
	usuario_criador varchar(200), 
    valor_total decimal(5,2) not null,
	constraint pk_pedido primary key (id)
);

create table estoque (
	id NUMBER GENERATED ALWAYS AS IDENTITY,
    gondola int default 0,
    armazenado int default 0,
    constraint pk_estoque primary key (id)
);

create table produto (
	id NUMBER GENERATED ALWAYS AS IDENTITY,
	nome varchar(200) not null,
	valor decimal(5,2) not null,
    id_estoque number not null,
	constraint pk_produto primary key (id)
);

alter table produto add constraint fk_estoque foreign key (id_estoque) references estoque(id);

create table item_pedido (
	id NUMBER GENERATED ALWAYS AS IDENTITY,
	quantidade int not null,
	id_pedido number not null,
	id_produto number not null,
	constraint pk_item_pedido primary key (id)
);

alter table item_pedido add constraint fk_item_pedido foreign key(id_pedido) references pedido(id);
alter table item_pedido add constraint fk_item_pedido_produto foreign key(id_produto) references produto(id);
