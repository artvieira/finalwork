create role gerente;
grant connect, resource, dba to gerente;
create user joaoamaral identified by 1;
grant gerente to joaoamaral;

create role atendente;
grant connect to atendente;
grant select, insert on system.pedido to atendente;
grant select, insert on system.item_pedido to atendente;
grant select on system.estoque_produto to atendente;
grant update on system.estoque to atendente;
create user arthurlucena identified by 1;
grant atendente to arthurlucena;