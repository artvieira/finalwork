insert into estoque (gondola, armazenado) values (5, 30);
insert into produto (nome, valor, id_estoque) values ('Ruffles', 8.99, (select max(id) from estoque));

insert into estoque (gondola, armazenado) values (10, 20);
insert into produto (nome, valor, id_estoque) values ('Coca-cola', 7.99, (select max(id) from estoque));

select * from produto;
select * from estoque;
