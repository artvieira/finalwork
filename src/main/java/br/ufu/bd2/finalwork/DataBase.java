package br.ufu.bd2.finalwork;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBase {

	private String urlBancoDados = "jdbc:oracle:thin:@localhost:1521/xe";
	private String user = "system";
	private String password = "oracle";
	private static Connection connection;
	
	public Connection getConnection(String user, String password) throws SQLException {
		this.user = user;;
		this.password = password;
		
		return getConnection();
	}

	public Connection getConnection() throws SQLException {
		if (connection == null || connection.isClosed()) { 
			connection = DriverManager.getConnection(urlBancoDados, user, password);
		}
		
		return connection;
	}
	
	public void closeConnection() throws SQLException {
		connection.close();
	}
	
	public void commit() throws SQLException {
		connection.commit();
	}
	
}
