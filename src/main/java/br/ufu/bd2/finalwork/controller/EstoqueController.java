package br.ufu.bd2.finalwork.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufu.bd2.finalwork.domain.Estoque;
import br.ufu.bd2.finalwork.repository.EstoqueRepository;

@RestController 
@RequestMapping(path="/estoque")
public class EstoqueController {	
	
	@Autowired
	private EstoqueRepository repositorio;
	
	@Autowired
	private HttpServletRequest context;
	
	@GetMapping
	public List<Estoque> buscar(@RequestParam Map<String, String> parametros) throws Exception {
		return repositorio.buscar(parametros.get("id"), getUsuario(), getSenha());
	}
	
	@GetMapping(path="/{id}")
	public Estoque buscar(@PathVariable("id") Long id) throws Exception {
		List<Estoque> estoques = repositorio.buscar(id.toString(), getUsuario(), getSenha());
		
		if (estoques.isEmpty()) {
			return null;
		}
		
		return estoques.get(0);
	}
	
	@PostMapping
	public Estoque novo(@RequestBody Estoque estoque) throws Exception {
		return repositorio.inserir(estoque, getUsuario(), getSenha());
	}

	@PutMapping(path="/{id}")
	public void editar(@PathVariable("id") Long id, @RequestBody Estoque estoque) throws Exception {
		repositorio.editar(id, estoque, getUsuario(), getSenha());
	}

	@DeleteMapping(path="/{id}")
	public void deletar(@PathVariable("id") Long id) throws Exception {
		repositorio.deletar(id, getUsuario(), getSenha());
	}
	
	private String getUsuario() {
		return context.getHeader("usuario");
	}
	
	private String getSenha() {
		return context.getHeader("senha");
	}
}