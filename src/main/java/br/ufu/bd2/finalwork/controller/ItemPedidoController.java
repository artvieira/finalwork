package br.ufu.bd2.finalwork.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufu.bd2.finalwork.domain.ItemPedido;
import br.ufu.bd2.finalwork.repository.ItemPedidoRepository;

@RestController 
@RequestMapping(path="/item-pedido")
public class ItemPedidoController {	
	@Autowired
	private ItemPedidoRepository repositorio;
	
	@Autowired
	private HttpServletRequest context;
	
	@GetMapping
	public List<ItemPedido> buscar(@RequestParam Map<String, String> parametros) throws Exception {
		return repositorio.buscar(parametros.get("id"), parametros.get("idPedido"), parametros.get("idEstoque"), getUsuario(), getSenha());
	}
	
	private String getUsuario() {
		return context.getHeader("usuario");
	}
	
	private String getSenha() {
		return context.getHeader("senha");
	}
}