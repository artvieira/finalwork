package br.ufu.bd2.finalwork.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufu.bd2.finalwork.domain.ItemPedido;
import br.ufu.bd2.finalwork.domain.Pedido;
import br.ufu.bd2.finalwork.repository.ItemPedidoRepository;
import br.ufu.bd2.finalwork.repository.PedidoRepository;
import br.ufu.bd2.finalwork.service.EstoqueService;

@RestController 
@RequestMapping(path="/pedido")
public class PedidoController {	
	@Autowired
	private PedidoRepository repositorio;
	
	@Autowired
	private ItemPedidoRepository itemPedidoRepositorio;
	
	@Autowired
	private EstoqueService estoqueService;
	
	@Autowired
	private HttpServletRequest context;
	
	@GetMapping
	public List<Pedido> buscar(@RequestParam Map<String, String> parametros) throws Exception {
		return repositorio.buscar(parametros.get("id"), parametros.get("usuario_criador"), getUsuario(), getSenha());
	}
	
	@GetMapping(path="/{id}")
	public Pedido buscar(@PathVariable("id") Long id) throws Exception {
		List<Pedido> pedidos = repositorio.buscar(id.toString(), null, getUsuario(), getSenha());
		
		if (pedidos.isEmpty()) {
			return null;
		}
		
		return pedidos.get(0);
	}
	
	@PostMapping
	public Pedido novo(@RequestBody Pedido pedidoTela) throws Exception {
		Pedido pedido = repositorio.inserir(pedidoTela, getUsuario(), getSenha());
		
		List<ItemPedido> itens = new ArrayList<>();
		
		Pedido pedidoId = new Pedido();
		pedidoId.setId(pedido.getId());
		
		for (ItemPedido itemPedido : pedidoTela.getItens()) {
			estoqueService.retirarGondola(itemPedido.getProduto().getId(), itemPedido.getQuantidade(), getUsuario(), getSenha());
			itemPedido.setPedido(pedidoId);
			itens.add(itemPedidoRepositorio.inserir(itemPedido, getUsuario(), getSenha()));
		}
		
		pedido.setItens(itens);

		return pedido;
	}

	@DeleteMapping(path="/{id}")
	public void deletar(@PathVariable("id") Long id) throws Exception {
		itemPedidoRepositorio.deletarPorPedido(id, getUsuario(), getSenha());
		repositorio.deletar(id, getUsuario(), getSenha());
	}
	
	private String getUsuario() {
		return context.getHeader("usuario");
	}
	
	private String getSenha() {
		return context.getHeader("senha");
	}
}