package br.ufu.bd2.finalwork.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufu.bd2.finalwork.domain.Estoque;
import br.ufu.bd2.finalwork.domain.Produto;
import br.ufu.bd2.finalwork.repository.EstoqueRepository;
import br.ufu.bd2.finalwork.repository.ProdutoRepository;

@RestController 
@RequestMapping(path="/produto")
public class ProdutoController {	
	@Autowired
	private ProdutoRepository repositorio;
	
	@Autowired
	private EstoqueRepository repositorioEstoque;
	
	@Autowired
	private HttpServletRequest context;
	
	@GetMapping
	public List<Produto> buscar(@RequestParam Map<String, String> parametros) throws Exception {
		return repositorio.buscar(parametros.get("id"), parametros.get("nome"), getUsuario(), getSenha());
	}
	
	@GetMapping(path="/{id}")
	public Produto buscar(@PathVariable("id") Long id) throws Exception {
		List<Produto> produtos = repositorio.buscar(id.toString(), null, getUsuario(), getSenha());
		
		if (produtos.isEmpty()) {
			return null;
		}
		
		return produtos.get(0);
	}
	
	@PostMapping
	public Produto novo(@RequestBody Produto produto) throws Exception {
		Estoque estoque = repositorioEstoque.inserir(new Estoque(null, 0l, 0l), getUsuario(), getSenha());
		produto.setEstoque(estoque);
		return repositorio.inserir(produto, getUsuario(), getSenha());
	}

	@PutMapping(path="/{id}")
	public void editar(@PathVariable("id") Long id, @RequestBody Produto produto) throws Exception {
		repositorio.editar(id, produto, getUsuario(), getSenha());
	}

	@DeleteMapping(path="/{id}")
	public void deletar(@PathVariable("id") Long id) throws Exception {
		repositorio.deletar(id, getUsuario(), getSenha());
	}
	
	private String getUsuario() {
		return context.getHeader("usuario");
	}
	
	private String getSenha() {
		return context.getHeader("senha");
	}
}