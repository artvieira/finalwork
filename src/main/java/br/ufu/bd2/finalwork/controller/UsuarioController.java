package br.ufu.bd2.finalwork.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufu.bd2.finalwork.DataBase;
import br.ufu.bd2.finalwork.domain.Usuario;

@RestController 
@RequestMapping(path="/usuario")
public class UsuarioController {	
	
	@PostMapping(path="/login")
	public List<String> buscar(@RequestBody Usuario usuario) throws Exception {
		List<String> retorno = new ArrayList<String>();
		
		DataBase db = new DataBase();
		
		Connection con = db.getConnection(usuario.getUsuario(), usuario.getSenha());
		
		try {
			String qs = "select granted_role from user_role_privs";
			
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				retorno.add(rs.getString("granted_role"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			con.close();
		}
		
		return retorno;
	}
}