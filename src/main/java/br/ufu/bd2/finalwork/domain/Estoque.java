package br.ufu.bd2.finalwork.domain;

public class Estoque {
	
	private Long id;
	private Long gondola;
	private Long armazenado;
	
	public Estoque() {}
	
	public Estoque(Long id, Long gondola, Long armazenado) {
		this.id = id;
		this.gondola = gondola;
		this.armazenado = armazenado;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public Long getGondola() {
		return gondola;
	}

	public void setGondola(Long gondola) {
		this.gondola = gondola;
	}

	public Long getArmazenado() {
		return armazenado;
	}

	public void setArmazenado(Long armazenado) {
		this.armazenado = armazenado;
	}

}
