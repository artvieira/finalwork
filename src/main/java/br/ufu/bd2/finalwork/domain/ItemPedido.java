package br.ufu.bd2.finalwork.domain;

public class ItemPedido {
	
	private Long id;
	private Long quantidade;
	private Produto produto;
	private Pedido pedido;
	
	public ItemPedido() {}
	
	public ItemPedido(Long id, Long quantidade, Long idProduto, Long idPedido) {
		this.id = id;
		this.quantidade = quantidade;
		this.produto = new Produto();
		this.produto.setId(idProduto);
		this.pedido = new Pedido();
		this.pedido.setId(idPedido);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	
	
	
}
