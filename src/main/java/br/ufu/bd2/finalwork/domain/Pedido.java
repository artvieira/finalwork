package br.ufu.bd2.finalwork.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Pedido {
	
	private Long id;
	private Date dataCriacao;
	private String usuarioCriador;
	private BigDecimal valorTotal;
	private List<ItemPedido> itens;
	
	public Pedido() {}
	
	public Pedido(Long id, Date dataCriacao, String usuarioCriador, BigDecimal valorTotal) {
		this.id = id;
		this.dataCriacao = dataCriacao;
		this.usuarioCriador = usuarioCriador;
		this.valorTotal = valorTotal;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getUsuarioCriador() {
		return usuarioCriador;
	}

	public void setUsuarioCriador(String usuarioCriador) {
		this.usuarioCriador = usuarioCriador;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public List<ItemPedido> getItens() {
		if (itens == null) {
			itens = new ArrayList<ItemPedido>();
		}
		
		return itens;
	}

	public void setItens(List<ItemPedido> itens) {
		this.itens = itens;
	}
	
}
