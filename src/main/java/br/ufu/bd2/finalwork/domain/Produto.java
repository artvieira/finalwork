package br.ufu.bd2.finalwork.domain;

import java.math.BigDecimal;

public class Produto {
	
	private Long id;
	private String nome;
	private BigDecimal valor;
	private Estoque estoque;
	
	public Produto() {}
	
	public Produto(Long id, String nome, BigDecimal valor, Long idEstoque) {
		this.id = id;
		this.nome = nome;
		this.valor = valor;
		this.estoque = new Estoque();
		this.estoque.setId(idEstoque);
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public BigDecimal getValor() {
		return valor;
	}
	
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Estoque getEstoque() {
		return estoque;
	}

	public void setEstoque(Estoque estoque) {
		this.estoque = estoque;
	}

}
