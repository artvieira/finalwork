package br.ufu.bd2.finalwork.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.ufu.bd2.finalwork.DataBase;
import br.ufu.bd2.finalwork.domain.Estoque;

@Service
public class EstoqueRepository {

	private final static String NOME_TABELA = "system.estoque";
	private final static String COLUNA_GONDOLA = "gondola";
	private final static String COLUNA_ARMAZENADO = "armazenado";
	
	public List<Estoque> buscar(String id, String usuario, String senha) throws Exception {
		List<Estoque> retorno = new ArrayList<Estoque>(); 
		String qs = "select * from " + NOME_TABELA + " where 1 = 1 ";
		int paramCount = 0;
		
		if (id != null) {
			qs += " and id = ? ";
			paramCount++;
		}
		
		DataBase db = new DataBase();

		Connection con = db.getConnection(usuario, senha);
		
		try {
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			if (id != null) {
				pstmt.setLong(paramCount, Long.valueOf(id));
				paramCount--;
			}
			
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				retorno.add(montarObjeto(rs));
			}
		} catch(Exception e) {
			throw e;
		} finally {
			con.close();
		}
		
		return retorno;
	}
	
	public Estoque inserir(Estoque estoque, String usuario, String senha) throws Exception {
		String qs = "insert into " + NOME_TABELA + " (" + COLUNA_GONDOLA + ", " + COLUNA_ARMAZENADO + ") values (?, ?)";
		
		DataBase db = new DataBase();

		Connection con = db.getConnection(usuario, senha);
		
		try {
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			pstmt.setLong(1, estoque.getGondola());
			pstmt.setLong(2, estoque.getArmazenado());
			
			pstmt.executeUpdate();
			
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select max(id) as id from " + NOME_TABELA);
			
			rs.next();
			
			estoque.setId(rs.getLong("id"));
			con.commit();
		} catch(Exception e) {
			throw e;
		} finally {
			con.close();
		}
		
		return estoque;
	}
	
	public void editar(Long id, Estoque estoque, String usuario, String senha) throws Exception {
		String qs = "update " + NOME_TABELA + " set " + COLUNA_GONDOLA + " = ?, " + COLUNA_ARMAZENADO + " = ? where id = ?";
		
		DataBase db = new DataBase();

		Connection con = db.getConnection(usuario, senha);
		
		try {
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			pstmt.setLong(1, estoque.getGondola());
			pstmt.setLong(2, estoque.getArmazenado());
			pstmt.setLong(3, id);
			
			pstmt.executeUpdate();
			con.commit();
		} catch(Exception e) {
			throw e;
		} finally {
			con.close();
		}
	}
	
	public void deletar(Long id, String usuario, String senha) throws Exception {
		String qs = "delete from " + NOME_TABELA + " where id = ?";
		
		DataBase db = new DataBase();

		Connection con = db.getConnection(usuario, senha);
		
		try {
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			pstmt.setLong(1, id);
			
			pstmt.executeUpdate();
			con.commit();
		} catch(Exception e) {
			throw e;
		} finally {
			con.close();
		}
		
	}
	
	public Estoque buscarPorProduto(Long idProduto, String usuario, String senha) throws SQLException {
		Estoque retorno = null;
		String qs = "select * from system.estoque_produto where id_produto = ? ";
		
		DataBase db = new DataBase();

		Connection con = db.getConnection(usuario, senha);
		
		try {
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			pstmt.setLong(1, idProduto);
			
			ResultSet rs = pstmt.executeQuery();
			
			rs.next();
			retorno = new Estoque(rs.getLong("id_estoque"), rs.getLong("gondola"), null);
		} catch(Exception e) {
			throw e;
		} finally {
			con.close();
		}
		
		return retorno;
	}
	
	public void alterarEstoqueGondola(Long id, Estoque estoque, String usuario, String senha) throws Exception {
		String qs = "update " + NOME_TABELA + " set " + COLUNA_GONDOLA + " = ? where id = ?";
		
		DataBase db = new DataBase();

		Connection con = db.getConnection(usuario, senha);
		
		try {
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			pstmt.setLong(1, estoque.getGondola());
			pstmt.setLong(2, id);
			
			pstmt.executeUpdate();
			con.commit();
		} catch(Exception e) {
			throw e;
		} finally {
			con.close();
		}
	}
	
	private Estoque montarObjeto(ResultSet rs) throws SQLException {
		return new Estoque(rs.getLong("id"), rs.getLong(COLUNA_GONDOLA), rs.getLong(COLUNA_ARMAZENADO));
	}
	
}
