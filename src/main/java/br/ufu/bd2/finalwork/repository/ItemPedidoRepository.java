package br.ufu.bd2.finalwork.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.ufu.bd2.finalwork.DataBase;
import br.ufu.bd2.finalwork.domain.ItemPedido;

@Service
public class ItemPedidoRepository {

	private final static String NOME_TABELA = "system.item_pedido";
	private final static String COLUNA_QUANTIDADE = "quantidade";
	private final static String COLUNA_ID_PEDIDO = "id_pedido";
	private final static String COLUNA_ID_PRODUTO = "id_produto";
	
	public List<ItemPedido> buscar(String id, String idPedido, String idProduto, String usuario, String senha) throws Exception {
		List<ItemPedido> retorno = new ArrayList<>(); 
		String qs = "select * from " + NOME_TABELA + " where 1 = 1 ";
		int paramCount = 0;
		
		if (id != null) {
			qs += " and id = ? ";
			paramCount++;
		}
		
		if (idPedido != null) {
			qs += " and " + COLUNA_ID_PEDIDO + " = ? ";
			paramCount++;
		}
		
		if (idProduto != null) {
			qs += " and " + COLUNA_ID_PRODUTO + " = ? ";
			paramCount++;
		}
		
		DataBase db = new DataBase();

		Connection con = db.getConnection(usuario, senha);
		
		try {
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			if (idProduto != null) {
				pstmt.setLong(paramCount, Long.valueOf(idProduto));
				paramCount--;
			}
			
			if (idPedido != null) {
				pstmt.setLong(paramCount, Long.valueOf(idPedido));
				paramCount--;
			}
			
			if (id != null) {
				pstmt.setLong(paramCount, Long.valueOf(id));
				paramCount--;
			}
			
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				retorno.add(montarObjeto(rs));
			}
		} catch(Exception e) {
			throw e;
		} finally {
			con.close();
		}
		
		return retorno;
	}
	
	public ItemPedido inserir(ItemPedido itemPedido, String usuario, String senha) throws Exception {
		String qs = "insert into " + NOME_TABELA + " (" + COLUNA_QUANTIDADE + ", " + COLUNA_ID_PRODUTO+ ", " + COLUNA_ID_PEDIDO + ") values (?, ?, ?)";
		
		DataBase db = new DataBase();

		Connection con = db.getConnection(usuario, senha);
		
		try {
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			pstmt.setLong(1, itemPedido.getQuantidade());
			pstmt.setLong(2, itemPedido.getProduto().getId());
			pstmt.setLong(3, itemPedido.getPedido().getId());
			
			pstmt.executeUpdate();
			
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select max(id) as id from " + NOME_TABELA);
			
			rs.next();
			
			itemPedido.setId(rs.getLong("id"));
			
			con.commit();
		} catch(Exception e) {
			throw e;
		} finally {
			con.close();
		}
		
		return itemPedido;
	}
	
	public void deletar(Long id, String usuario, String senha) throws Exception {
		String qs = "delete from " + NOME_TABELA + " where id = ?";
		
		DataBase db = new DataBase();

		Connection con = db.getConnection(usuario, senha);
		
		try {
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			pstmt.setLong(1, id);
			
			pstmt.executeUpdate();
			con.commit();
		} catch(Exception e) {
			throw e;
		} finally {
			con.close();
		}
		
	}
	
	public void deletarPorPedido(Long id, String usuario, String senha) throws SQLException {
		String qs = "delete from " + NOME_TABELA + " where " + COLUNA_ID_PEDIDO + " = ?";
		
		DataBase db = new DataBase();

		Connection con = db.getConnection(usuario, senha);
		
		try {
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			pstmt.setLong(1, id);
			
			pstmt.executeUpdate();
			con.commit();
		} catch(Exception e) {
			throw e;
		} finally {
			con.close();
		}
	}
	
	private ItemPedido montarObjeto(ResultSet rs) throws SQLException {
		return new ItemPedido(rs.getLong("id"), rs.getLong(COLUNA_QUANTIDADE), rs.getLong(COLUNA_ID_PRODUTO), rs.getLong(COLUNA_ID_PEDIDO));
	}
	
}
