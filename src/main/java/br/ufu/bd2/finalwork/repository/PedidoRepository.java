package br.ufu.bd2.finalwork.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.ufu.bd2.finalwork.DataBase;
import br.ufu.bd2.finalwork.domain.Pedido;

@Service
public class PedidoRepository {

	private final static String NOME_TABELA = "system.pedido";
	private final static String COLUNA_DATA_CRIACAO = "data_criacao";
	private final static String COLUNA_USUARIO_CRIADOR = "usuario_criador";
	private final static String COLUNA_VALOR_TOTAL = "valor_total";
	
	public List<Pedido> buscar(String id, String usuarioCriador, String usuario, String senha) throws Exception {
		List<Pedido> retorno = new ArrayList<>(); 
		String qs = "select * from " + NOME_TABELA + " where 1 = 1 ";
		int paramCount = 0;
		
		if (id != null) {
			qs += " and id = ? ";
			paramCount++;
		}
		
		if (usuarioCriador != null) {
			qs += " and " + COLUNA_USUARIO_CRIADOR + " = ? ";
			paramCount++;
		}
		
		DataBase db = new DataBase();

		Connection con = db.getConnection(usuario, senha);
		
		try {
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			if (usuarioCriador != null) {
				pstmt.setString(paramCount, usuarioCriador);
				paramCount--;
			}
			
			if (id != null) {
				pstmt.setLong(paramCount, Long.valueOf(id));
				paramCount--;
			}
			
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				retorno.add(montarObjeto(rs));
			}
		} catch(Exception e) {
			throw e;
		} finally {
			con.close();
		}
		
		return retorno;
	}
	
	public Pedido inserir(Pedido pedido, String usuario, String senha) throws Exception {
		String qs = "insert into " + NOME_TABELA + " (" + COLUNA_VALOR_TOTAL + ") values (?)";
		
		DataBase db = new DataBase();

		Connection con = db.getConnection(usuario, senha);
		
		try {
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			pstmt.setBigDecimal(1, pedido.getValorTotal());
			
			pstmt.executeUpdate();
			
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select max(id) as id from " + NOME_TABELA);
			
			rs.next();
			
			pedido.setId(rs.getLong("id"));
			
			con.commit();
		} catch(Exception e) {
			throw e;
		} finally {
			con.close();
		}
		
		return pedido;
	}
	
	public void deletar(Long id, String usuario, String senha) throws Exception {
		String qs = "delete from " + NOME_TABELA + " where id = ?";
		
		DataBase db = new DataBase();

		Connection con = db.getConnection(usuario, senha);
		
		try {
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			pstmt.setLong(1, id);
			
			pstmt.executeUpdate();
			con.commit();
		} catch(Exception e) {
			throw e;
		} finally {
			con.close();
		}
		
	}
	
	private Pedido montarObjeto(ResultSet rs) throws SQLException {
		return new Pedido(rs.getLong("id"), rs.getDate(COLUNA_DATA_CRIACAO), rs.getString(COLUNA_USUARIO_CRIADOR), rs.getBigDecimal(COLUNA_VALOR_TOTAL));
	}
}
