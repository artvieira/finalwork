package br.ufu.bd2.finalwork.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.ufu.bd2.finalwork.DataBase;
import br.ufu.bd2.finalwork.domain.Produto;

@Service
public class ProdutoRepository {

	private final static String NOME_TABELA = "system.produto";
	private final static String COLUNA_NOME = "nome";
	private final static String COLUNA_VALOR = "valor";
	private final static String COLUNA_ID_ESTOQUE = "id_estoque";
	
	public List<Produto> buscar(String id, String nome, String usuario, String senha) throws Exception {
		List<Produto> retorno = new ArrayList<Produto>(); 
		String qs = "select * from " + NOME_TABELA + " where 1 = 1";
		int paramCount = 0;
		
		if (id != null) {
			qs += " and id = ? ";
			paramCount++;
		}
		
		if (nome != null) {
			qs += " and LOWER(" + COLUNA_NOME + ") like LOWER(?) ";
			paramCount++;
		}
		
		DataBase db = new DataBase();

		Connection con = db.getConnection(usuario, senha);
		
		try {
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			if (nome != null) {
				pstmt.setString(paramCount, "%" + nome + "%");
				paramCount--;
			}
			
			if (id != null) {
				pstmt.setLong(paramCount, Long.valueOf(id));
				paramCount--;
			}
			
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next()) {
				retorno.add(montarObjeto(rs));
			}
		} catch(Exception e) {
			throw e;
		} finally {
			con.close();
		}
		
		return retorno;
	}
	
	public Produto inserir(Produto produto, String usuario, String senha) throws Exception {
		String qs = "insert into " + NOME_TABELA + " (" + COLUNA_NOME + ", " + COLUNA_VALOR + ", " + COLUNA_ID_ESTOQUE + ") values (?, ?, ?)";
		
		DataBase db = new DataBase();

		Connection con = db.getConnection(usuario, senha);
		
		try {
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			pstmt.setString(1, produto.getNome());
			pstmt.setBigDecimal(2, produto.getValor());
			pstmt.setLong(3, produto.getEstoque().getId());
			
			pstmt.executeUpdate();
			
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select max(id) as id from " + NOME_TABELA);
			
			rs.next();
			
			produto.setId(rs.getLong("id"));
			con.commit();
			
			con.commit();
		} catch(Exception e) {
			throw e;
		} finally {
			con.close();
		}
		
		return produto;
	}
	
	public void editar(Long id, Produto produto, String usuario, String senha) throws Exception {
		String qs = "update " + NOME_TABELA + " set " + COLUNA_NOME + " = ?, " + COLUNA_VALOR + " = ? where id = ?";
		
		DataBase db = new DataBase();

		Connection con = db.getConnection(usuario, senha);
		
		try {
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			pstmt.setString(1, produto.getNome());
			pstmt.setBigDecimal(2, produto.getValor());
			pstmt.setLong(3, id);
			
			pstmt.executeUpdate();
			con.commit();
		} catch(Exception e) {
			throw e;
		} finally {
			con.close();
		}
	}
	
	public void deletar(Long id, String usuario, String senha) throws Exception {
		String qs = "delete from " + NOME_TABELA + " where id = ?";
		
		DataBase db = new DataBase();

		Connection con = db.getConnection(usuario, senha);
		
		try {
			PreparedStatement pstmt = con.prepareStatement(qs);
			
			pstmt.setLong(1, id);
			
			pstmt.executeUpdate();
			con.commit();
		} catch(Exception e) {
			throw e;
		} finally {
			con.close();
		}
		
	}
	
	private Produto montarObjeto(ResultSet rs) throws SQLException {
		return new Produto(rs.getLong("id"), rs.getString(COLUNA_NOME), rs.getBigDecimal(COLUNA_VALOR), rs.getLong(COLUNA_ID_ESTOQUE));
	}
}
