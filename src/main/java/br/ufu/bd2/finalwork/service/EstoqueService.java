package br.ufu.bd2.finalwork.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufu.bd2.finalwork.domain.Estoque;
import br.ufu.bd2.finalwork.repository.EstoqueRepository;

@Service
public class EstoqueService {

	@Autowired
	private EstoqueRepository repositorio;
	
	public void retirarGondola(Long idProduto, Long qtdRetirar, String usuario, String senha) throws Exception {
		Estoque estoque = repositorio.buscarPorProduto(idProduto, usuario, senha);
		
		if (estoque.getGondola() - qtdRetirar < 0) {
			throw new Exception("Não existe quantidade sufiente!");
		}
		
		estoque.setGondola(estoque.getGondola() - qtdRetirar);
		
		repositorio.alterarEstoqueGondola(estoque.getId(), estoque, usuario, senha);
	}
}
